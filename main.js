const WIDTH = 640;
const HEIGHT = 480;
let cvs = document.getElementById('cvs');
let ctx = cvs.getContext('2d');
cvs.width = WIDTH;
cvs.height = HEIGHT;
let gravity = 2;
let bg = new Background(WIDTH, HEIGHT);
let user = new User();
let birdy = new Bird(HEIGHT);
let puppies = [];
puppies.push(new Pipe(WIDTH, HEIGHT));

window.onload = () => {
	let game = new Game(WIDTH, HEIGHT);
	// game.init();
	game.start();
}

function random(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }

let lastTime = 0;

let gameLoop = function(timestamp){
	let deltaTime = lastTime - timestamp;
	lastTime = timestamp;
	ctx.clearRect(0,0, WIDTH, HEIGHT);

	bg.draw(ctx, timestamp);
	bg.update();

	for(i=puppies.length-1; i>=0; i--){
		puppies[i].drawTop(ctx);
		puppies[i].drawBtm(ctx);
		puppies[i].update();

		if (puppies[i].nextPipe()){
		puppies.push(new Pipe(WIDTH, HEIGHT));
		puppies[i].nextOneCalled = true;
		}
		if (puppies[i].offScreen()) {
			puppies.splice(i,1);
		}
			birdy.wellDone(puppies[i]);
			birdy.hitting(puppies[i]);
	}


	birdy.maxSpeed();
	birdy.update(gravity, deltaTime);
	birdy.inBox();
	birdy.draw(ctx);
	// console.log(birdy.birdState)
	// birdy.up();
	// console.log(birdy.yv);

	requestAnimationFrame(gameLoop);
}

window.addEventListener("keydown", (event) => {
	if (event.keyCode == 38 || event.keyCode == 32) {
			birdy.up();
	}
})
// gameLoop();
