class Pipe{
	constructor(gameWidth, gameHeight){
		this.width = 80;
		this.x = gameWidth+40;
		this.spacing = random(150, 300);
		this.speed = 3;
		this.nextOneCalled = false;
		this.scored = false;
		this.topPipe = {
			position: {
				y: 0,
				x: this.x
			},
			length: random(10, gameHeight-this.spacing - 10)
		};
		this.btmPipe = {
			position: {
				y: this.topPipe.length + this.spacing,
				x: this.x
			},
			length: gameHeight-this.topPipe.length - this.spacing
		};
		this.topPipeImg = new Image(this.width, this.topPipe.length);
		this.topPipeImg.src = "img/pipe1.png";
		this.btmPipeImg = new Image(this.width, this.btmPipe.length);
		this.btmPipeImg.src = "img/pipe1.png";
	}
	drawTop(ctx){
		// console.log(this.btmPipe);
		/*ctx.fillStyle = "#195105";
		ctx.fillRect(this.x, this.topPipe.position.y, this.width, this.topPipe.length);*/
		ctx.drawImage(this.topPipeImg, this.x, this.topPipe.position.y, this.width, this.topPipe.length);
	}
	drawBtm(ctx){
		/*ctx.fillStyle = "#195105";
		ctx.fillRect(this.x, this.btmPipe.position.y, this.width, this.btmPipe.length);*/
		ctx.drawImage(this.btmPipeImg, this.x, this.btmPipe.position.y, this.width, this.btmPipe.length);
	}
	update(){
		this.x-=this.speed;
	}
	nextPipe(){
		if (!this.nextOneCalled) return (this.x<WIDTH/2)? true:false;
	}
	offScreen(){
		return (this.x < -this.width); 
	}
}