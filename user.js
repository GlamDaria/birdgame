class User{
	constructor(name){
		this.name = name;
		this._score = 0;
	}
	get score() {
		return this._score;
	}
	set score(n){
		this._score=n;
	}
}