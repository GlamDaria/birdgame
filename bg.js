class Background{
	constructor(gameWidth, gameHeight){
		this.gameWidth =gameWidth;
		this.width = 1887;
		this.height = 811;
		this.position = {
			x: 0,
			y:0
		};
		this.bgImg = new Image(this.width, this.height);
		this.bgImg.src = "img/bg.jpg";
		this.speed = 0.1;
	};
	draw(ctx){
		ctx.drawImage(this.bgImg, this.position.x, this.position.y, this.width, this.height);
		if(this.position.x<=-this.width+this.gameWidth){
			console.log(this.position.x);
			ctx.drawImage(this.bgImg, this.position.x+this.width, this.position.y, this.width, this.height);
		}
	}
	update(){
		this.position.x-=this.speed;
		/*if(this.position.x<-this.width){
			console.log(this.position.x);
			this.position.x+=this.width+this.gameWidth;
		}*/
		/*if(this.position.x<=-this.width+this.gameWidth){
			console.log(this.position.x);
			this.position.x+=this.width;
		}*/
	}
}